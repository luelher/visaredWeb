Rails.application.routes.draw do
  resources :devices
  resources :tags
  devise_for :users

  root 'dashboard#index'

  namespace :api do
    namespace :v1 do
        resources :tag_values, :tags, :commands, :defaults => { :format => 'json' }
    end
  end

  get 'telecontrol', to: 'telecontrols#index'
  post 'telecontrol', to: 'telecontrols#create'
  get 'perfil', to: 'clients#edit'
  post 'perfil', to: 'clients#update'
  get 'configuration', to: 'configuration#index'
  get 'alerts', to: 'tag_alerts#index'


end

