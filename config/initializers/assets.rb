# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
Rails.application.config.assets.paths << Rails.root.join("app", "assets", "template").to_s

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
#Rails.application.config.assets.precompile += %w( applicationw.css )
#Rails.application.config.assets.precompile += %w( applicationw.js )
Rails.application.config.assets.precompile += %w( *.png )

# Css of template
Rails.application.config.assets.precompile += %w( plugins/pace/pace-theme-flash.css )
Rails.application.config.assets.precompile += %w( plugins/bootstrap/css/bootstrap.min.css )
Rails.application.config.assets.precompile += %w( plugins/bootstrap/css/bootstrap-theme.min.css )
Rails.application.config.assets.precompile += %w( fonts/font-awesome/css/font-awesome.css )
Rails.application.config.assets.precompile += %w( css/animate.min.css )
Rails.application.config.assets.precompile += %w( plugins/perfect-scrollbar/perfect-scrollbar.css )
Rails.application.config.assets.precompile += %w( plugins/icheck/skins/square/orange.css )
Rails.application.config.assets.precompile += %w( css/style.css )
Rails.application.config.assets.precompile += %w( css/responsive.css )
Rails.application.config.assets.precompile += %w( plugins/morris-chart/css/morris.css )
Rails.application.config.assets.precompile += %w( plugins/ios-switch/css/switch.css )

Rails.application.config.assets.precompile += %w( plugins/datatables/css/jquery.dataTables.css )
Rails.application.config.assets.precompile += %w( plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css )
Rails.application.config.assets.precompile += %w( plugins/datatables/extensions/Responsive/css/dataTables.responsive.css )
Rails.application.config.assets.precompile += %w( plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.css )
# Rails.application.config.assets.precompile += %w( .css )



#Js of template
Rails.application.config.assets.precompile += %w( js/jquery-1.11.2.min.js )
Rails.application.config.assets.precompile += %w( js/jquery.easing.min.js )
Rails.application.config.assets.precompile += %w( plugins/bootstrap/js/bootstrap.min.js )
Rails.application.config.assets.precompile += %w( plugins/pace/pace.min.js )
Rails.application.config.assets.precompile += %w( plugins/perfect-scrollbar/perfect-scrollbar.min.js )
Rails.application.config.assets.precompile += %w( plugins/viewport/viewportchecker.js )
Rails.application.config.assets.precompile += %w( plugins/icheck/icheck.min.js )
Rails.application.config.assets.precompile += %w( js/scripts.js )
Rails.application.config.assets.precompile += %w( plugins/sparkline-chart/jquery.sparkline.min.js )
Rails.application.config.assets.precompile += %w( js/chart-sparkline.js )
Rails.application.config.assets.precompile += %w( plugins/morris-chart/js/raphael-min.js )
Rails.application.config.assets.precompile += %w( plugins/morris-chart/js/morris.min.js )
Rails.application.config.assets.precompile += %w( plugins/jquery-ui/smoothness/jquery-ui.min.js )
Rails.application.config.assets.precompile += %w( plugins/datepicker/js/datepicker.js )
Rails.application.config.assets.precompile += %w( plugins/daterangepicker/js/moment.min.js )
Rails.application.config.assets.precompile += %w( plugins/daterangepicker/js/daterangepicker.js )
Rails.application.config.assets.precompile += %w( plugins/timepicker/js/timepicker.min.js )
Rails.application.config.assets.precompile += %w( plugins/datetimepicker/js/datetimepicker.min.js )
Rails.application.config.assets.precompile += %w( plugins/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js )
Rails.application.config.assets.precompile += %w( plugins/colorpicker/js/bootstrap-colorpicker.min.js )
Rails.application.config.assets.precompile += %w( plugins/tagsinput/js/bootstrap-tagsinput.min.js )
Rails.application.config.assets.precompile += %w( plugins/select2/select2.min.js )
Rails.application.config.assets.precompile += %w( plugins/typeahead/typeahead.bundle.js )
Rails.application.config.assets.precompile += %w( plugins/typeahead/handlebars.min.js )
Rails.application.config.assets.precompile += %w( plugins/multi-select/js/jquery.multi-select.js )
Rails.application.config.assets.precompile += %w( plugins/multi-select/js/jquery.quicksearch.js )
Rails.application.config.assets.precompile += %w( plugins/tagsinput/js/bootstrap-tagsinput.min.js.map )
Rails.application.config.assets.precompile += %w( fonts/font-awesome/fonts/fontawesome-webfont.woff@v=4.2.0 )

Rails.application.config.assets.precompile += %w( plugins/jquery-validation/js/jquery.validate.min.js )
Rails.application.config.assets.precompile += %w( plugins/jquery-validation/js/additional-methods.min.js )
Rails.application.config.assets.precompile += %w( js/form-validation.js )

Rails.application.config.assets.precompile += %w( plugins/datatables/js/jquery.dataTables.min.js )
Rails.application.config.assets.precompile += %w( plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js )
Rails.application.config.assets.precompile += %w( plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js )
Rails.application.config.assets.precompile += %w( plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js )



