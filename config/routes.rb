Rails.application.routes.draw do  
  get 'signals/index'

  

  devise_for :users
  resources :tag_types
  resources :devices
  resources :tags
  

  root 'dashboard#index'

  namespace :api do  
    namespace :v1 do
        resources :tag_values, :tags, :commands, :defaults => { :format => 'json' }
    end    
  end  

  get 'telecontrol', to: 'telecontrols#index'
  post 'telecontrol', to: 'telecontrols#create'
  get 'perfil', to: 'clients#edit'
  post 'perfil', to: 'clients#update'
  get 'configuration', to: 'configuration#index'
  put 'tag_alerts/:id' => 'tag_alerts#update'
  get 'tag_alerts' => 'tag_alerts#show'
  get 'tag_notices' => 'tag_alerts#show_notices'
  put 'tag_notices/:id' => 'tag_alerts#update_notices'
  get 'signals/detail/:id', to: 'signals#detail'
  

end
