class ActionCorreo < ApplicationMailer
	def bienvenido_email(client)
		@client = client	  
	  	email_with_name1 =@client.email_1
	  	email_with_name2 =@client.email_2
	  	mail(to: [email_with_name1, email_with_name2], subject: 'Configuracion de Cliente en VisaredWeb')
	end

	def enviar_alertas_email(tag_alerts, client)
		@tag_alerts = tag_alerts
		#byebug
		@client = client
		if 	 @client.email 
			email_with_name1 =@client.email_1
			email_with_name2 =@client.email_2
			mail(to: [email_with_name1, email_with_name2], subject: 'Alertas y Notificaciones en VisaredWeb')
		end
	end
end