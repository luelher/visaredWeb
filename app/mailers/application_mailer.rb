class ApplicationMailer < ActionMailer::Base
  default from: "marquezdigna83@gmail.com"
  layout 'mailer'

end
