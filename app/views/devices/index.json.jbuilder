json.array!(@devices) do |device|
  json.extract! device, :id, :place, :serial, :ip,  :model_id, :client_id
  json.url device_url(device, format: :json)
end
