json.array!(@tags) do |tag|
  json.extract! tag, :id, :name, :tag_type_id, :location, :port, :config, :device_id
  json.url tag_url(tag, format: :json)
end
