/*delegate click*/

function listaralertas(){
    $.ajax({
        url: "/tag_alerts",
        dataType: "JSON",
        timeout: 30000,
        beforeSend: function(){
           $(".alertas").html('<div><p class="list-group-item-text">Cargando<p></div>');
        },
        error: function(){
           $(".alertas").html('<div><p class="list-group-item-text">Error<p></div>');
        },
        success: function(res){
        	var cadena ='<a href="#" class="list-group-item list-group-item-danger"><h4 class="list-group-item-heading">Alarmas</h4></a>';
        	$.each(res, function(index) {        		
	             //console.log(res.length);
	            if(res[index].description){              
				  cadena = cadena + '<div id='+res[index].id+' class=""><a href="#" class="list-group-item alert fa fa-close" ><p class="list-group-item-text">'+res[index].description+'</p></a></div>';
			    }else{
				  cadena = cadena + ' No hay Alertas ';
				}
        	});
        	//console.log(cadena);
        	$(".alertas").html(cadena);
        	$(".count_alert").text(res.length);

        }
     });
};

function listarnoticias(){
    $.ajax({
        url: "/tag_notices",
        dataType: "JSON",
        timeout: 30000,
        beforeSend: function(){
           $(".notice").html('<div><p class="list-group-item-text">Cargando<p></div>');
        },
        error: function(){
           $(".notice").html('<div><p class="list-group-item-text">Error<p></div>');
        },
        success: function(res){
        	var cadena ='<a href="#" class="list-group-item list-group-item-danger"><h4 class="list-group-item-heading">Noticias</h4></a>';
        	$.each(res, function(index) {        		
	             //console.log(res.length);
	            if(res[index].description){              
				  cadena = cadena + '<div id='+res[index].id+' class=""><a href="#" class="list-group-item notice_click fa fa-close" ><p class="list-group-item-text">'+res[index].description+'</p></a></div>';
			    }else{
				  cadena = cadena + ' No hay Alertas ';
				}
        	});
        	//console.log(cadena);
        	$(".notice").html(cadena);
        	$(".count_notice").text(res.length);

        }
     });
};

function veralertas() {
	 $(".alertas").delegate('.alert','click',function (){ 
	/*$(".alert").click(function () {*/
		console.log('aqui entro');
		var id = $(this).parent().get(0).id;
		$("#" + id).remove();
			$.ajax({
			        type: "PUT",
				        dataType: "script",
				        url: '/tag_alerts/' + id,
				        data:
				        {            
				            datos : { viewed: true}
				        }
				    }).done(function( msg )
				        {
				            //alert( "Alerta Vista: " + msg );
				            $(".count_alert").text(msg);
				        });
	});

};

function veranoticias() {
	 $(".notice").delegate('.notice_click','click',function (){ 
	/*$(".alert").click(function () {*/
		console.log('aqui entro');
		var id = $(this).parent().get(0).id;
		$("#" + id).remove();
			$.ajax({
			        type: "PUT",
				        dataType: "script",
				        url: '/tag_notices/' + id,
				        data:
				        {            
				            datos : { viewed: true}
				        }
				    }).done(function( msg )
				        {
				            //alert( "Alerta Vista: " + msg );
				            $(".count_notice").text(msg);
				        });
	});

};

$(document).ready(function(){

	listaralertas();
    veralertas(); 
    listarnoticias();
    veranoticias();


    //telecontrols
    setInterval(update_data, 30000);
    update_data();   

});