var app = angular.module("app", ["ngResource", "ngRadialGauge", "angular-toArrayFilter", "chart.js", 'toggle-switch']);

//Order by Iterating an Object of Objects (Acting as an Associative Array or Hash)
app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});
