// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//= require jquery
//= require jquery_ujs
//= require js/jquery-1.11.2.min
//= require jquery.easing
//= require bootstrap-sprockets
//= require plugins/pace/pace.min
//= require perfect-scrollbar
//= require plugins/viewport/viewportchecker
//= require plugins/morris-chart/js/raphael-min
//= require plugins/morris-chart/js/morris.min
//= require icheck

//= require plugins/jquery-ui/smoothness/jquery-ui.min
//= require plugins/datepicker/js/datepicker
//= require plugins/daterangepicker/js/moment.min
//= require plugins/daterangepicker/js/daterangepicker
//= require plugins/timepicker/js/timepicker.min
//= require plugins/datetimepicker/js/datetimepicker.min
//= require plugins/datetimepicker/js/locales/bootstrap-datetimepicker.fr
//= require plugins/colorpicker/js/bootstrap-colorpicker.min
//= require plugins/tagsinput/js/bootstrap-tagsinput.min
//= require plugins/select2/select2.min
//= require plugins/typeahead/typeahead.bundle
//= require plugins/typeahead/handlebars.min
//= require plugins/multi-select/js/jquery.multi-select
//= require plugins/multi-select/js/jquery.quicksearch

//= require plugins/jquery-validation/js/jquery.validate.min
//= require plugins/jquery-validation/js/additional-methods.min
//= require js/form-validation

//= require plugins/datatables/js/jquery.dataTables.min
//= require plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min
//= require plugins/datatables/extensions/Responsive/js/dataTables.responsive.min
//= require plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap

//= require plugins/sparkline-chart/jquery.sparkline.min
//= require js/chart-sparkline
//= require js/scripts
//= require d3
//= require angular
//= require angular-resource
//= require ng-directivas/angular-toggle-switch.min
//= require plugins/chartjs-chart/Chart
//= require ng-directivas/ng-radial-gauge-dir
//= require ng-directivas/toArrayFilter
//= require ng-directivas/angular-chart
//= require config/app
//= require config/data
//= require ng-controllers/dashi
//= require ng-controllers/signalsCtrl
//= require ng-controllers/signaldetailCtrl
//= require telecontrols
//= require apevents


/*delegate click*/
// function listaralertas(){
//     $.ajax({
//         url: "/tag_alerts",
//         dataType: "JSON",
//         timeout: 30000,
//         beforeSend: function(){
//            $(".alertas").html('<div><p class="list-group-item-text">Cargando<p></div>');
//         },
//         error: function(){
//            $(".alertas").html('<div><p class="list-group-item-text">Error<p></div>');
//         },
//         success: function(res){
//         	var cadena ='<a href="#" class="list-group-item list-group-item-danger"><h4 class="list-group-item-heading">Alarmas</h4></a>';
//         	$.each(res, function(index) {        		
// 	             //console.log(res.length);
// 	            if(res[index].description){              
// 				  cadena = cadena + '<div id='+res[index].id+' class=""><a href="#" class="list-group-item alert fa fa-close" ><p class="list-group-item-text">'+res[index].description+'</p></a></div>';
// 			    }else{
// 				  cadena = cadena + ' No hay Alertas ';
// 				}
//         	});
//         	//console.log(cadena);
//         	$(".alertas").html(cadena);
//         	$(".count_alert").text(res.length);

//         }
//      });
// };

// function listarnoticias(){
//     $.ajax({
//         url: "/tag_notices",
//         dataType: "JSON",
//         timeout: 30000,
//         beforeSend: function(){
//            $(".notice").html('<div><p class="list-group-item-text">Cargando<p></div>');
//         },
//         error: function(){
//            $(".notice").html('<div><p class="list-group-item-text">Error<p></div>');
//         },
//         success: function(res){
//         	var cadena ='<a href="#" class="list-group-item list-group-item-danger"><h4 class="list-group-item-heading">Noticias</h4></a>';
//         	$.each(res, function(index) {        		
// 	             //console.log(res.length);
// 	            if(res[index].description){              
// 				  cadena = cadena + '<div id='+res[index].id+' class=""><a href="#" class="list-group-item notice_click fa fa-close" ><p class="list-group-item-text">'+res[index].description+'</p></a></div>';
// 			    }else{
// 				  cadena = cadena + ' No hay Alertas ';
// 				}
//         	});
//         	//console.log(cadena);
//         	$(".notice").html(cadena);
//         	$(".count_notice").text(res.length);

//         }
//      });
// };

// function veralertas() {
// 	 $(".alertas").delegate('.alert','click',function (){ 
// 	/*$(".alert").click(function () {*/
// 		console.log('aqui entro');
// 		var id = $(this).parent().get(0).id;
// 		$("#" + id).remove();
// 			$.ajax({
// 			        type: "PUT",
// 				        dataType: "script",
// 				        url: '/tag_alerts/' + id,
// 				        data:
// 				        {            
// 				            datos : { viewed: true}
// 				        }
// 				    }).done(function( msg )
// 				        {
// 				            //alert( "Alerta Vista: " + msg );
// 				            $(".count_alert").text(msg);
// 				        });
// 	});

// };

// function veranoticias() {
// 	 $(".notice").delegate('.notice_click','click',function (){ 
// 	/*$(".alert").click(function () {*/
// 		console.log('aqui entro');
// 		var id = $(this).parent().get(0).id;
// 		$("#" + id).remove();
// 			$.ajax({
// 			        type: "PUT",
// 				        dataType: "script",
// 				        url: '/tag_notices/' + id,
// 				        data:
// 				        {            
// 				            datos : { viewed: true}
// 				        }
// 				    }).done(function( msg )
// 				        {
// 				            //alert( "Alerta Vista: " + msg );
// 				            $(".count_notice").text(msg);
// 				        });
// 	});

// };

// $(document).ready(function(){

// 	listaralertas();
//     veralertas(); 
//     listarnoticias();
//     veranoticias();   
//     //angular.bootstrap(document, ['app']);
//  });


