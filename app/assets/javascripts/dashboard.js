

$(document).ready( function() {

  mChart_temp = new Morris.Line({
    element: 'tag-1',
    xkey: 'tiempo',
    ykeys: ['valor'],
    labels: ['Valor']
  });

  mChart_hume = new Morris.Line({
    element: 'tag-2',
    xkey: 'tiempo',
    ykeys: ['valor'],
    labels: ['Valor']
  });

  mChart_temp_2 = new Morris.Line({
    element: 'tag-4',
    xkey: 'tiempo',
    ykeys: ['valor'],
    labels: ['Valor']
  });


  //Definition of the function (non-global, because of the previous line)
  function update_charts(){

    $.getJSON( "/api/v1/tag_values?page_size=20&tag_id=1", function( json ) {
      
      var chart_data = [];

      for (var i = 0; i < json.length; i++) {
          chart_data.push({ tiempo: json[i].collection_time_humanized, valor: json[i].value });
      }

      mChart_temp.setData(chart_data);
      
    });


    $.getJSON( "/api/v1/tag_values?page_size=20&tag_id=2", function( json ) {
      
      var chart_data = [];

      for (var i = 0; i < json.length; i++) {
          chart_data.push({ tiempo: json[i].collection_time_humanized, valor: json[i].value });
      }

      mChart_hume.setData(chart_data);
      
    });

    $.getJSON( "/api/v1/tag_values?page_size=1&tag_id=3", function( json ) {

      if(json[0].value=="1"){
        $('#tag-3').prop("checked", "checked");
      }else{
        $('#tag-3').prop("checked", "");
      }
      $('#tag-3-time').html("Al: "+json[0].collection_time_ago_in_words);

    });

    $.getJSON( "/api/v1/tag_values?page_size=20&tag_id=4", function( json ) {
      
      var chart_data = [];

      for (var i = 0; i < json.length; i++) {
          chart_data.push({ tiempo: json[i].collection_time_humanized, valor: json[i].value });
      }

      mChart_temp_2.setData(chart_data);
      
    });

    
  }

  //set an interval
  setInterval(update_charts, 30000);

  //Call the function
  update_charts();
});


