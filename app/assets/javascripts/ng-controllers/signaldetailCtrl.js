app.controller('signaldetailCtrl', function ($scope, Data, $log, $location, $timeout, $filter) {
 	$scope.mensaje = 'Data Load ';
 	$scope.signals = {};
 	/*$scope.labels = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto"];
	$scope.series = ['Series A', 'Series B'];
	$scope.data = [
		[65, 59, 80, 81, 56, 55, 40, 39, 45],
		[28, 48, 40, 19, 86, 27, 90, 75, 81]
	];*/
	$scope.labels = [];
	$scope.series = [];
	$scope.data = [];


	$scope.$watch('idsignal', function () {
	    id = $scope.idsignal;
	    Data.get('/api/v1/tags/'+id).then(function (data) {
    		$scope.signal = data.name;
    		$scope.tag_values = data.tag_values;
    		$scope.location = data.location;
    		//$log.log($scope.data);
    		//$log.log($scope.signals);
    		//$log.log($scope.tag_values);
    		var values = {name: 'misko', gender: 'male'};
    		var log = [];
    		var yvalor = [];
    		var labels = [];
    		angular.forEach($scope.tag_values, function(value, key) {
    		  this.push(key + ': ' + value.value);
    		  $scope.labels.push(value.id+' ('+$filter('date')(new Date(value.collection_time_humanized), "mediumDate")+')');
    		  
    		  //$scope.labels.push(new Date(value.collection_time_humanized).getMonth());
    		  yvalor.push(value.value);

    		  
    		}, log);
    		//$scope.labels.push(labelss);
    		//$scope.labels.push(labels);
    		$scope.series.push(data.tag_type.name);
    		$scope.data.push(yvalor);
    	});
	    $log.log('event id signal: '+id);
	    //$scope.mensaje = 'Id signal refresh: '+id;
	});
	$scope.onClick = function (points, evt) {
		var idpoints = points[0].label.split(' (');
		var id = idpoints[0];
		$log.log('Tag Values ID: ('+id[0]+')');

	    Data.get('/api/v1/tag_values/'+id).then(function (data) {
    		$scope.tag_value_detail = data;
    		//$log.log($scope.data);
    		//$log.log($scope.signals);
    		//$log.log($scope.tag_values);
    		
    	});
		//console.log(points);
		//console.log(evt);
	};

	$scope.onDetail= function(){
		$log.log('Event on Signal Detail');
		window.location = '/signals/detail';
		$scope.idsignal = 'Provider view Signal Index For Detail';
	};

});