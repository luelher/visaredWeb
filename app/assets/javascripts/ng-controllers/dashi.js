app.controller('dashCtrl', function ($scope, Data, $log, $location, $timeout) {
 	$scope.mensaje = 'Data Load ';
 	
 	loaddata();
    function update() {
        $timeout(function() {
            loaddata();
            update();
        }, 30000);
    }
    update();

    function loaddata(){
    	Data.get('/api/v1/tags').then(function (data) {
    			$scope.tags = data
    			configInitGauge();
    			//get_tags_values_last(data);
    		});
    }
    /*function update() {
        $timeout(function() {
            $scope.value=$scope.value + 0.1;
            if ($scope.value > $scope.upperLimit) {
                $scope.value=$scope.lowerLimit;
            }
            update();
        }, 1000);
    }
    update();*/
	id = '1';

	/*function get_tags_values_last(data){
		var log = [];
		angular.forEach(data, function(value, key) {
			this.push(key + ': ' + value.name);
		}, log);
		$log.log(log);
		Data.get('/api/v1/tag_values?page_size=20&tag_id=' + id).then(function (data) {
			
		});
	}*/
	function configInitGauge(){

		   $scope.upperLimit = 100;
		   $scope.lowerLimit = 0;
		   $scope.warningValue = 244;
		   $scope.dangerValue = 240;
		   $scope.normalValue = 260;
		   $scope.unit = " °C";
		   $scope.precision = 2;
		   var min_normal_value =  $scope.warningValue + 1;
		   		max_normal_value = $scope.normalValue;
		   		max_warning_value = $scope.warningValue;
		   		min_warning_value = $scope.dangerValue + 1;
		   		max_danger_value = $scope.dangerValue;
		   		min_danger_value = $scope.dangerValue - 10;
		   		color_warning = '#FDC702';
		   		color_danger = '#C50200'
		   		color_normal = '#8DCA2F'

		   $scope.ranges = [
		       {
		           min: 0,
		           max: 60,
		           color: '#DEDEDE'
		       },
		       {
		           min: 60,
		           max: min_danger_value - 1,
		           color: '#DEDEDE'
		       },
		       {
		           min: min_danger_value,
		           max: max_danger_value,
		           color: color_danger
		       },
		       {
		           min: min_warning_value,
		           max: max_warning_value,
		           color: color_warning
		       },
		       {
		           min: min_normal_value,
		           max: max_normal_value,
		           color: color_normal
		       }
		   ];
	}

	/*angular.element(document).ready(function () {
        console.log("COlsoao");
    });*/

	/*var values = {name: 'misko', gender: 'male'};
	var log = [];
	angular.forEach(values, function(value, key) {
	  this.push(key + ': ' + value);
	}, log);
	$log.log(log);*/
	//expect(log).toEqual(['name: misko', 'gender: male']);
});