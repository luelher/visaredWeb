class Client < ActiveRecord::Base
  has_many :user  
  has_many :device  
  validates :email_1,
    length: { maximum: 255 },
    presence: true
  validates :phone_1,
    length: { maximum: 11 },
    presence: true
end
