class Tag < ActiveRecord::Base
  last_value = nil

  belongs_to :tag_type
  belongs_to :device
  has_one :client, through: :device
  has_many :tag_values

  scope :for_client, ->(client_id){ joins(:device).where("devices.client_id = ?",client_id)}
  scope :just_actuators, ->(){ joins(:tag_type).where("tag_types.tag_type = ?",TagType::ACTUATOR)}

  def last_value
    last_value ||= tag_values.last
  end

end
