class Command < ActiveRecord::Base
  belongs_to :device
  has_one :client, through: :device

  scope :for_client, ->(client_id){ joins(:device).where("devices.client_id = ?",client_id)}

  WRITE_PORT = 1

  def type_name
    case command_type
    when WRITE_PORT
      "Escribir Puerto"
    else
      "Desconocido"
    end
  end
end
