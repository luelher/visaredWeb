class TagValue < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  after_create :add_tag_alert

  belongs_to :tag
  belongs_to :tag_alert
  before_create :add_report_time

  def as_json(options={})
    opts = super(options={})
    opts.merge!(tag_alert: tag_alert)
    opts.merge!(collection_time_ago_in_words: collection_time_ago_in_words)
    opts.merge!(collection_time_humanized: collection_time.to_formatted_s(:db))
  end

  def collection_time_ago_in_words
    time_ago_in_words(collection_time)
  end  
  def tag_alert
    tag_alert ||= TagAlert.for_tag_value(self.id)
  end

  def self.today
    where("collection_time >= ?", Time.zone.now.beginning_of_day)
  end
  def self.week
    where(:collection_time => Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
  end
  def self.last_month # Show only products of last month.
   where(:collection_time => 1.month.ago.beginning_of_month..1.month.ago.end_of_month)
  end

  def self.this_month # Show only products of this month.
     where(:collection_time => Date.today.beginning_of_month..Date.today.end_of_month)
  end
  
private 
  def add_report_time
    self.report_time = DateTime.now
  end

public

  def add_tag_alert

    normal = tag.normal_value
    warning = tag.warning_value
    danger = tag.danger_value
    humbral = tag.humbral
    info = warning + ((normal - warning) * (humbral/100.00) )


    if (value >= warning) and (value <= info)
      TagAlert.create :type_alert =>"Info" , :description =>"Informacion la " + tag.tag_type.name + ": #{value}, se encuentra en el humbral (#{warning} - #{info}) ", :viewed => 'false', :sent => "mails", :processed => "true", :tag_value_id => id      
    end

    if (value >= danger) and (value < warning)
      TagAlert.create :type_alert =>"Warning" , :description =>"Advertencia la " + tag.tag_type.name + ": #{value}, se encuentra en el humbral (#{danger} - #{warning}) ", :viewed => 'false', :sent => "mails", :processed => "true", :tag_value_id => id      
    end  

    if (value < danger) 
      TagAlert.create :type_alert =>"Danger" , :description =>"Peligro la " + tag.tag_type.name + ": #{value}, se encuentra por debajo de #{danger}  ", :viewed => 'false', :sent => "mails", :processed => "true", :tag_value_id => id      
    end  
   
  end

end
