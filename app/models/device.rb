class Device < ActiveRecord::Base
  belongs_to :model
  belongs_to :client  
  has_many :tags


  def full_name
    "#{model.name} - #{serial} - ip: #{ip}"
  end
end
