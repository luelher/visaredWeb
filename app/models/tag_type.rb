class TagType < ActiveRecord::Base
  belongs_to :tags

  RANK = "rank"
  ACTUATOR = "actuator"

  def is_rank?
    tag_type == RANK
  end

  def is_actuator?
    tag_type == ACTUATOR
  end

end
