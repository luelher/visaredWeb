class Tag < ActiveRecord::Base
  last_value = nil
  tag_alert = nil
  belongs_to :tag_type
  belongs_to :device
  has_one :client, through: :device
  has_many :tag_values

  validates :name,
  length: { maximum: 255 },
  presence: true
  validates :tag_type,
  presence: true
  validates :location,
  length: { maximum: 255 },
  presence: true
  validates :port,
  length: { maximum: 10 },
  presence: true
  validates :config,
  length: { maximum: 100 },
  presence: true
   validates :device_id,
  length: { maximum: 10 },
  presence: true

  scope :for_client, ->(client_id){ joins(:device).where("devices.client_id = ?",client_id)}
  scope :just_actuators, ->(){ joins(:tag_type).where("tag_types.tag_type = ?",TagType::ACTUATOR)}

  def last_value
    last_value ||= tag_values.last
  end
  def tag_alert
    tag_alert ||= TagAlert.for_tag_value(tag_values.last)
  end

  def as_json(options={})
    respond = super.as_json(options).merge({
        tag_type: tag_type,
        device: device,
        last_value: tag_values.last,
        tag_values: tag_values,
        tag_value_now: tag_values.today,
        tag_value_week: tag_values.week,
        tag_value_month: tag_values.last_month,
        tag_alert: TagAlert.for_tag_value(tag_values.last)
    })
  end

end
