class User < ActiveRecord::Base
	belongs_to :client
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable



  def full_name
    "#{name} #{last_name}"
  end
end
