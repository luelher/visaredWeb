class TagAlert < ActiveRecord::Base
	belongs_to :tag_value
	scope :for_tag_value, ->(id){ where("tag_alerts.tag_value_id = ?",id)}
end
