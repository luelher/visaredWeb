class DashboardController < ApplicationController
  before_action :authenticate_user!

  def index
    @tags_of_client = Tag.for_client(current_user.client_id)
  end
end
