class TelecontrolsController < ApplicationController
  before_action :authenticate_user!

  def index
    @tags_of_client = Tag.for_client(current_user.client_id).just_actuators
    @commands = Command.for_client(current_user.client_id).all
  end

  def create

    tag_id = nil
    params.each do |k,v|
      tag = k.split("-")
      if tag.count == 3
        tag_id = tag[1]
      end
    end
    if tag_id
      tag = Tag.find(tag_id)
      if params["tag-#{tag_id}"] 
        value = true
      else
        value = false
      end
      if tag
        cmd = Command.create(port: tag.port, command_type: Command::WRITE_PORT, command: "io.digitalWrite", value: (value ? "io.HIGH" : "io.LOW"), executed: false, device_id: tag.device_id)
        if(cmd)
          flash["success"] = "Comando Creado y Enviado a ejecución"
        else
          flash["warning"] = "No se pudo crear el comando"
        end
      end
    end

    return redirect_to action: "index"
  end

end
