
class ClientsController < ApplicationController
	before_action :set_clients, only: [:edit, :update]
	#before_action :authenticate_user!

	def edit
	end

	def update
    respond_to do |format|
      #byebug
      if @client.update(clients_params)
        # Llamamos al   ActionMailer que creamos
          ActionCorreo.bienvenido_email(@client).deliver
        # @tag_alerts = TagAlert.joins(tag_value: {tag: :device}).where("devices.client_id = ? and viewed=false", current_user.client_id) 
        # ActionCorreo.enviar_alertas_email(@tag_alerts, @client).deliver
        format.html { render :update, notice: 'Post was successfully updated.' }
        format.json { render :update, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
	end

    private    
    def set_clients    	
      @client = Client.find(current_user.client_id)      
    end
   
    def clients_params
      params.require(:client).permit(:email, :phone, :email_1, :email_2, :phone_1, :phone_2, :notifications, :alerts)
    end
end
