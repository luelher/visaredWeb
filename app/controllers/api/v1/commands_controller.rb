class Api::V1::CommandsController < Api::V1::BaseController
    
    respond_to :json

    # GET /api/{plural_resource_name}
    def index  
      plural_resource_name = "@#{resource_name.pluralize}"
      if(query_params[:device_id])
        resources = resource_class.where(query_params)
                                  .page(page_params[:page])
                                  .per(page_params[:page_size])

        instance_variable_set(plural_resource_name, resources)
        respond_with instance_variable_get(plural_resource_name)
      else
        render json: {}, status: :not_found
      end
    end

    private

      def command_params
        params.require(:command).permit(:port, :command_type, :command, :value, :executed, :device_id)
      end

      def query_params
        params.permit(:device_id, :executed)
      end

end
