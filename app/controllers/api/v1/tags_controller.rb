class Api::V1::TagsController < Api::V1::BaseController
    
    respond_to :json

    # GET /api/{plural_resource_name}
    def index  
      plural_resource_name = "@#{resource_name.pluralize}"
      if params['client_id'] && params['device_id']
        resources = resource_class.joins(:device, :tag_type).where('devices.client_id = ? and device_id = ?', params['client_id'].to_i, params['device_id'].to_i)
                                  .page(page_params[:page])
                                  .per(page_params[:page_size])        
      else
        resources = resource_class.where(query_params)
                                  .page(page_params[:page])
                                  .per(page_params[:page_size])                
      end
      instance_variable_set(plural_resource_name, resources)
      respond_with instance_variable_get(plural_resource_name)
    end

    private

      def tag_value_params
        params.require(:tag).permit(:name, :tag_type_id, :location, :port, :config)
      end

      def query_params
        params.permit(:client_id, :name, :id, :device_id)
      end

end
