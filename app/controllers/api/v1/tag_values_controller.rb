class Api::V1::TagValuesController < Api::V1::BaseController
    
    respond_to :json

    # GET /api/{plural_resource_name}
    def index  
    	plural_resource_name = "@#{resource_name.pluralize}"
        if params['last']
	        resources = resource_class.where(query_params)
	                                .page(page_params[:page])
	                                .per(page_params[:page_size])
	                                .order('collection_time ASC').last(params['last'].to_i) 
	    else
	    	resources = resource_class.where(query_params)
	    	                        .page(page_params[:page])
	    	                        .per(page_params[:page_size]) 
	    end
	    instance_variable_set(plural_resource_name, resources)
    	respond_with instance_variable_get(plural_resource_name)
    end
    private

      def tag_value_params
        params.require(:tag_value).permit(:tag_id, :value, :collection_time, :collection_time_humanized)
      end

      def query_params
        params.permit(:tag_id, :value, :collection_time, :collection_time_humanized, :id)
      end

end
