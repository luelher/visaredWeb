class TagAlertsController < ApplicationController
  
  # PATCH/PUT /tag_alerts/1
  # PATCH/PUT /tag_alerts/1.json
  def update
     @tag_alert = TagAlert.find(params[:id])

    respond_to do |format|      
      if @tag_alert.update_attributes(viewed: 'true')  
          @tag_alerts = TagAlert.joins(tag_value: {tag: :device}).where("devices.client_id = ? and viewed=false and type_alert in ('Warning','Danger')", current_user.client_id)    
          @count_alert = @tag_alerts.count       
          format.json {  render :json => @count_alert }      
      end
    end

  end

  def update_notices
     @tag_alert = TagAlert.find(params[:id])

    respond_to do |format|      
      if @tag_alert.update_attributes(viewed: 'true')  
          @tag_notices = TagAlert.joins(tag_value: {tag: :device}).where("devices.client_id = ? and viewed=false and type_alert in ('Info')", current_user.client_id)    
          @count_notice = @tag_notices.count       
          format.json {  render :json => @count_notice }      
      end
    end

  end

  def show
      @tag_alerts = TagAlert.joins(tag_value: {tag: :device}).where("devices.client_id = ? and viewed=false and type_alert in ('Warning','Danger') ", current_user.client_id)           
      render :json => @tag_alerts
  end

  def show_notices
      @tag_notices = TagAlert.joins(tag_value: {tag: :device}).where("devices.client_id = ? and viewed=false and type_alert in ('Info') ", current_user.client_id)           
      render :json => @tag_notices
  end

end
