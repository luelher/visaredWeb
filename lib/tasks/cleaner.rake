namespace :cleaner do
  desc "Limpiar Registros"
  task clean: :environment do

    def do_clean
      if TagAlert.count > 10000
        TagAlert.order('created_at DESC').limit(TagAlert.count - 8000).destroy_all
      end

      if TagValue.count > 10000
        TagValue.order('created_at DESC').limit(TagValue.count - 8000).destroy_all
      end
    end
  
  end

end