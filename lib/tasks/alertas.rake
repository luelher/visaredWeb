namespace :alertas do
  desc "TODO"
  task generar_alertas: :environment do

  	def listar_alertas	
    	@client = Client.find(current_user.client_id)      	
  		@tag_alerts = TagAlert.joins(tag_value: {tag: :device}).where("devices.client_id = ? and viewed=false", current_user.client_id)	
  		ActionCorreo.enviar_alertas_email(@tag_alerts, @client).deliver
		  #byebug		
	   end
  
  end

end


=begin

    En base a la configuración del cliente, se debe verificar si tiene activado el envío de notificaciones por mail y sms. (estos campos son parte de las opciones de configuración del cliente, pueden ser campos bool. Por defecto estas dos opciones son true)
    En base a lo que el cliente tenga seleccionado se hará el proceso de envío de la notificación.
    Se colocará un texto descriptivo sencillo
    Se le mostrarán los valores del sensor.

=end