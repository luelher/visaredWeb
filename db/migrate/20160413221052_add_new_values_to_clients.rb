class AddNewValuesToClients < ActiveRecord::Migration
  def change
    add_column :clients, :email_1, :string
    add_column :clients, :email_2, :string
    add_column :clients, :phone_1, :string
    add_column :clients, :phone_2, :string
    add_column :clients, :notifications, :integer
    add_column :clients, :alerts, :integer
  end
end
