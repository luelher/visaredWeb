class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.references :tag_type, index: true, foreign_key: true
      t.string :location
      t.string :port
      t.string :config
      t.references :device, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
