class AddValuesToTagAlert < ActiveRecord::Migration
  def change
  	add_column :tags, :normal_value, :integer
    add_column :tags, :warning_value, :integer
    add_column :tags, :danger_value, :integer
    add_column :tags, :humbral, :integer
  end
end
