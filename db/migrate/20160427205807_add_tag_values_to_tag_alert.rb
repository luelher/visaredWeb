class AddTagValuesToTagAlert < ActiveRecord::Migration
  def change  	
  	add_reference :tag_alerts, :tag_value, index: true, foreign_key: true
  	#add_foreign_key :tag_alerts, :tag_values, index: true
  end
end
