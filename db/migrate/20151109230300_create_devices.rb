class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :place
      t.string :serial
      t.string :ip
      t.references :model, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true


      t.timestamps null: false
    end
  end
end
