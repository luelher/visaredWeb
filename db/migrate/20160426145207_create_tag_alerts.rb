class CreateTagAlerts < ActiveRecord::Migration
  def change
    create_table :tag_alerts do |t|
      t.string :type_alert
      t.string :description
      t.boolean :viewed
      t.string :sent
      t.boolean :processed

      t.timestamps null: false
    end
  end
end
