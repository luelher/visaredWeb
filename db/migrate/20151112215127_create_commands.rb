class CreateCommands < ActiveRecord::Migration
  def change
    create_table :commands do |t|
      t.string :port
      t.integer :command_type
      t.string :command
      t.string :value
      t.boolean :executed, default: false
      t.references :device, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
