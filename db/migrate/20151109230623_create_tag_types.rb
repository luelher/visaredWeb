class CreateTagTypes < ActiveRecord::Migration
  def change
    create_table :tag_types do |t|
      t.string :name
      t.string :tag_type
      t.string :command
      t.boolean :status

      t.timestamps null: false
    end
  end
end
