class CreateTagValues < ActiveRecord::Migration
  def change
    create_table :tag_values do |t|
      t.references :tag, index: true, foreign_key: true
      t.integer :value
      t.datetime :collection_time
      t.datetime :report_time

      t.timestamps null: false
    end
  end
end
