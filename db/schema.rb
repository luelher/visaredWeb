# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160502134038) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.integer  "client_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "email_1"
    t.string   "email_2"
    t.string   "phone_1"
    t.string   "phone_2"
    t.integer  "notifications"
    t.integer  "alerts"
    t.boolean  "email"
    t.boolean  "phone"
  end

  create_table "commands", force: :cascade do |t|
    t.string   "port"
    t.integer  "command_type"
    t.string   "command"
    t.string   "value"
    t.boolean  "executed",     default: false
    t.integer  "device_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "commands", ["device_id"], name: "index_commands_on_device_id", using: :btree

  create_table "devices", force: :cascade do |t|
    t.string   "place"
    t.string   "serial"
    t.string   "ip"
    t.integer  "model_id"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "devices", ["client_id"], name: "index_devices_on_client_id", using: :btree
  add_index "devices", ["model_id"], name: "index_devices_on_model_id", using: :btree

  create_table "models", force: :cascade do |t|
    t.string   "name"
    t.string   "brand"
    t.string   "model"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tag_alerts", force: :cascade do |t|
    t.string   "type_alert"
    t.string   "description"
    t.boolean  "viewed"
    t.string   "sent"
    t.boolean  "processed"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "tag_value_id"
  end

  add_index "tag_alerts", ["tag_value_id"], name: "index_tag_alerts_on_tag_value_id", using: :btree

  create_table "tag_types", force: :cascade do |t|
    t.string   "name"
    t.string   "tag_type"
    t.string   "command"
    t.boolean  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tag_values", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "value"
    t.datetime "collection_time"
    t.datetime "report_time"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "tag_values", ["tag_id"], name: "index_tag_values_on_tag_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.integer  "tag_type_id"
    t.string   "location"
    t.string   "port"
    t.string   "config"
    t.integer  "device_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "normal_value"
    t.integer  "warning_value"
    t.integer  "danger_value"
    t.integer  "humbral"
  end

  add_index "tags", ["device_id"], name: "index_tags_on_device_id", using: :btree
  add_index "tags", ["tag_type_id"], name: "index_tags_on_tag_type_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "client_id"
    t.string   "name"
    t.string   "last_name"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["client_id"], name: "index_users_on_client_id", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "commands", "devices"
  add_foreign_key "devices", "clients"
  add_foreign_key "devices", "models"
  add_foreign_key "tag_alerts", "tag_values"
  add_foreign_key "tag_values", "tags"
  add_foreign_key "tags", "devices"
  add_foreign_key "tags", "tag_types"
  add_foreign_key "users", "clients"
end
