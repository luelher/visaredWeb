# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


client = Client.create(:name => 'Admin', :client_type => 1, :email_1 => 'email1', :email_2 => 'email2', :phone_1 => 'phone_1', :phone_2 => 'phone_2', :notifications => '20', :alerts => '30')

user_admin = User.create(:email => 'admin@visared.net', :password => '12345678', :client_id => client.id, :name => 'Luis', :last_name => 'Aguilar')
user_demo = User.create(:email => 'demo@visared.net', :password => '12345678', :client_id => client.id, :name => 'Alejandro', :last_name => 'Mella')

model = Model.create(:name => 'Raspberry Pi', :brand => 'Raspberry Pi', :model => '2014')

device = Device.create(:place => 'Patio de Tanques', :model_id => model.id, :client_id => client.id, :serial => '123123123', :ip => '192.168.0.99')

tag_type = TagType.create(
                              :name => "Temperatura Directa",
                              :tag_type => "rank",
                              :command => "Adafruit_DHT.read_retry",
                              :status => "TRUE"
                          )


tag_type_2 = TagType.create(
                              :name => "Humedad",
                              :tag_type => "rank",
                              :command => "Adafruit_DHT.read_retry",
                              :status => "TRUE"
                          )

tag_type_3 = TagType.create(
                              :name => "Actuador",
                              :tag_type => "actuator",
                              :command => "io.digitalRead",
                              :status => "TRUE"
                          )

tag_type_4 = TagType.create(
                              :name => "Temperatura cable",
                              :tag_type => "rank",
                              :command => "tfile.read",
                              :status => "TRUE"
                          )


tag = Tag.create(
                  :name => "Temperatura Cuarto Servidores", 
                  :tag_type_id => tag_type.id,
                  :device_id => device.id,
                  :location => "Cuato servidores, planta baja",
                  :port => 25,
                  :config => "Adafruit_DHT.DHT11",
                  :normal_value => 260,
                  :warning_value => 244,
                  :danger_value => 240,
                  :humbral => 10
                )


tag_2 = Tag.create(
                  :name => "Humedad Cuarto Servidores", 
                  :tag_type_id => tag_type_2.id,
                  :device_id => device.id,
                  :location => "Cuato servidores, planta baja",
                  :port => 25,
                  :config => "Adafruit_DHT.DHT11",
                  :normal_value => 260,
                  :warning_value => 244,
                  :danger_value => 240,
                  :humbral => 10
                )

tag_3 = Tag.create(
                  :name => "Actuador Normalmente Abierto ", 
                  :tag_type_id => tag_type_3.id,
                  :device_id => device.id,
                  :location => "Cuato servidores, planta baja",
                  :port => 7,
                  :config => "GPIO.BOARD",
                  :normal_value => 260,
                  :warning_value => 244,
                  :danger_value => 240,
                  :humbral => 10
                )

tag_4 = Tag.create(
                  :name => "Temperatura Cuarto Servidores Cable", 
                  :tag_type_id => tag_type_4.id,
                  :device_id => device.id,
                  :location => "Cuato servidores, planta baja",
                  :port => 0,
                  :config => "/sys/bus/w1/devices/28-021564d7b3ff/w1_slave",
                  :normal_value => 260,
                  :warning_value => 244,
                  :danger_value => 240,
                  :humbral => 10
                )


TagValue.create(
                  :tag_id => tag.id,
                  :value => 24,
                  :collection_time => DateTime.now,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag.id,
                  :value => 20,
                  :collection_time => DateTime.now - 2.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag.id,
                  :value => 18,
                  :collection_time => DateTime.now - 3.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag.id,
                  :value => 22,
                  :collection_time => DateTime.now - 4.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag.id,
                  :value => 16,
                  :collection_time => DateTime.now - 5.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag.id,
                  :value => 23,
                  :collection_time => DateTime.now - 6.minutes,
                  :report_time => (DateTime.now)
                )

# Humedad
TagValue.create(
                  :tag_id => tag_2.id,
                  :value => 24-5,
                  :collection_time => DateTime.now,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag_2.id,
                  :value => 20-5,
                  :collection_time => DateTime.now - 2.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag_2.id,
                  :value => 18-5,
                  :collection_time => DateTime.now - 3.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag_2.id,
                  :value => 22-5,
                  :collection_time => DateTime.now - 4.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag_2.id,
                  :value => 16-5,
                  :collection_time => DateTime.now - 5.minutes,
                  :report_time => (DateTime.now)
                )

TagValue.create(
                  :tag_id => tag_2.id,
                  :value => 23-5,
                  :collection_time => DateTime.now - 6.minutes,
                  :report_time => (DateTime.now)
                )

# Actuador
TagValue.create(
                  :tag_id => tag_3.id,
                  :value => 1,
                  :collection_time => DateTime.now - 6.minutes,
                  :report_time => (DateTime.now)
                )

Command.create(:command => ' io.digitalWrite', :port => '7', :command_type => '1', :value => 'io.HIGH', :device_id => device.id)